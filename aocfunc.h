#define TRUE 1
#define FALSE 0

struct aoc {
    char *filename; //filename of puzzle input
    int N;          //size of puzzle inpus
    int *pidata;     //the puzzle input data
    char **pistring;
};

struct aoc_arrays {
    int N;          //number of elements in array
    char **string;   //string in the array
} ;

struct aoc_arrayd {
    int N;          //number of elements in array
    int *data;      //data in the array
} ;


void aocinit(struct aoc *pi); /*initializes arrays in the aoc struct*/

void aocfree(struct aoc *pi); /*frees the arrays in struct aoc*/

void aoc_arrays_init(struct aoc_arrays *array); /*initialize a string array struct*/

void aoc_arrays_free(struct aoc_arrays *array); /*free the string array struct*/

void aoc_arrayd_init(struct aoc_arrayd *array);

void aoc_arrayd_free(struct aoc_arrayd *array);

void readarrayd(struct aoc *pi); /*reads an array of int from text file. Returns the number of ints*/

void readarrays(struct aoc *pi); /*reads an array of strings*/

int bin2dec(char *binary, int size); /* little endian binary to decimal converter*/

int sumarrayd(int *array, int N); /*sums up all ints in array of size N*/

int** matrixallocd(int rows, int cols); /* allocate memory to a matrix of ints */

void matrixfreed(int **matrix, int rows, int cols); /* free matrix memory */

void readcsvlines(char* filename, int line, int readlines, struct aoc_arrays *array); /* reads in READLINES lines of CSV data starting from line LINE. 
                                                                            The aoc_arrays struct holds the string .string and number of 
                                                                            of strings .N. You will need to initialize with aoc_arrays_init and free
                                                                            with aoc_arrays_free*/

void readcsvlined(char* filename, int line, int readlines, struct aoc_arrayd *array);

void arrays2arrayd(char **string, int *integers, int arraysize); /*converts an array of strings to ints*/

void skiplines(FILE *fp, int Nskip); /*advance the file pointer Nskip lines*/

void gotoline(FILE *fp, int lineno); /*advance fp to a particular linenumber is a file*/

void readNstrings(char *filename, char **data, int startline, int Nstrings); /*skips to line startline and then reads in Nstrings strings to data array*/