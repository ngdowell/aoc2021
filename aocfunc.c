#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "aocfunc.h"

#define TRUE 1
#define FALSE 0
#define PI 3.141592653589793238

int** matrixallocd(int rows, int cols) {
    int **matrix;
    int i;
    matrix=(int**)malloc(sizeof (int*)*rows);
    for (i=0; i<rows; i++) matrix[i]=(int*)malloc(sizeof(int)*cols);
    return matrix;
}

void matrixfreed(int **matrix, int rows, int cols) {
    int i;
    for (i=0; i<rows; i++) free(matrix[i]);
    free(matrix);
}


void aocinit(struct aoc *pi) {
    pi->pidata=NULL;
    pi->pistring=NULL;
}

void aocfree(struct aoc *pi) {
    int i;
    if (pi->pidata!=NULL) free(pi->pidata);
    if (pi->pistring!=NULL) {
        for (i=0;i<pi->N; i++) free(pi->pistring[i]);
        free(pi->pistring);
    }
}

void aoc_arrays_init(struct aoc_arrays *array) {
    array->string=NULL;
    array->N=0;
}

void aoc_arrayd_init(struct aoc_arrayd *array) {
    array->data=NULL;
    array->N=0;
}

void aoc_arrayd_free(struct aoc_arrayd *array) {
    if (array->data!=NULL) free(array->data);
}

void aoc_arrays_free(struct aoc_arrays *array) {
    int i;
    if (array->string!=NULL) {
        for (i=0;i<array->N; i++) free(array->string[i]);
        free(array->string);
    }
}

void readarrayd(struct aoc *pi) {
    FILE *fp;
    int i;
    int tmp=0;

    if ((fp=fopen(pi->filename,"r"))==NULL) {
        printf("ERROR: file cannnot be opened\n");
        exit(1);
    }

    /*get number of entries in file*/
    pi->N=0;
    while (fscanf(fp,"%d",&tmp)==1) pi->N++;
    rewind(fp);

    /*allocate memory*/
    pi->pidata=(int*)malloc(pi->N*sizeof(int));

    /*put input into array*/
    for (i=0; i<pi->N; i++) fscanf(fp,"%d",&pi->pidata[i]);

    fclose(fp);
}

void readarrays(struct aoc *pi) {
    FILE *fp;
    int i;
    const int Nchar=500; //maximum word size
    char tmp[500];

    if ((fp=fopen(pi->filename,"r"))==NULL) {
        printf("ERROR: file cannnot be opened\n");
        exit(1);
    }

    /*get number of entries in file*/
    pi->N=0;
    while (fscanf(fp,"%s",tmp)==1) pi->N++;
    rewind(fp);

    /*allocate memory -assume each string is < Nchar chars*/
    pi->pistring=(char**)malloc(pi->N*sizeof(char*));
    for (i=0; i<pi->N; i++) pi->pistring[i]=(char*)malloc(sizeof(char)*Nchar);

    /*put input into array*/
    for (i=0; i<pi->N; i++) fscanf(fp,"%s",pi->pistring[i]);

    fclose(fp);
}

int bin2dec(char *binary, int size) {
    int decimal=0;
    int i;
    int bit;

    for (i=0; i<size; i++) { 
        if (binary[size-1-i]=='0') bit=0;
        if (binary[size-1-i]=='1') bit=1;
        decimal+=bit*pow(2,i);
    }
    return decimal;
}

int sumarrayd(int *array, int N) {
    int i;
    int sum=0;
    for (i=0; i<N; i++) sum=sum+array[i];
    return sum;
}

void readcsvlines(char* filename, int line, int readlines, struct aoc_arrays *array) {
FILE *fp;
    int i;
    int pos=0, word=0;
    char ch,lastch;
    int nrows=0,ncols,lastncols;
    int nlines=0;
    int Nchar=5000; //maximum size for string


    if ((fp=fopen(filename,"r"))==NULL) {
        printf("ERROR: file cannnot be opened\n");
        exit(1);
    }

    /*count total number of lines in file*/
    ch=getc(fp);
    while(ch!=EOF){
        if (ch=='\n') nlines++;
        ch=getc(fp);
    }
    nlines++; //add line for where EOF comes.

/*catch attempts to read beyond end of file*/
    if (line>nlines) {
        printf("ERROR: file only contains %d lines. You want to read %d. \n",nlines,line);
        exit(1);
    }

    /*go to requested line*/
    gotoline(fp,line);

    /*count rows and cols in the requested segment of file - 
        check it is a valid CSV segment (i.e. each row has the same ncols)*/
    ncols=0;
    ch=getc(fp);
    while(nrows<readlines){
        if ((ch=='\n')) {
            nrows++;
            ncols++;
            if (nrows>1) {
                if (lastncols!=ncols) {
                    printf("WARNING: ncols different for rows\n");
                    aoc_arrays_free(array);
                    fclose(fp);
                    exit(1);
                }
            }
            lastncols=ncols;
            ncols=0;
        }
        else if (ch==',') {
            ncols++;
        }
        lastch=ch;
        ch=getc(fp);
        if ((ch==EOF) && (lastch!='\n')) {  //last row has no line break
            nrows++;
            ncols++;
            if (nrows>1) {
                if (lastncols!=ncols) {
                    printf("WARNING: ncols different for rows\n");
                    aoc_arrays_free(array);
                    fclose(fp);
                    exit(1);
                }
            }
        }
        if ((ch==EOF) && (lastch=='\n')) ncols=lastncols; //last row does have a line break (ignore the new blank line)
        if (nrows==readlines) ncols=lastncols;
    } 

    printf("csv has %d rows, %d cols\n",nrows,ncols);
    array->N=nrows*ncols;

    /*allocate memory to the strings*/
    array->string=(char**)malloc(sizeof(char*)*array->N);
    for (i=0; i<array->N; i++) array->string[i]=(char*)malloc(sizeof(char)*Nchar);

    /*go to requested line*/
    gotoline(fp,line);

    /*fill up the array*/
    ch=getc(fp);
    nrows=0;
    while((nrows<readlines) && (ch!=EOF)){
        if (ch==',' || ch=='\n') {
            array->string[word][pos]='\0';
            word++;
            if (ch=='\n') nrows++;
            pos=0;
        }
        else {
            array->string[word][pos]=ch;
            pos++;
        }
        lastch=ch;
        ch=getc(fp);
        if ((ch==EOF) && (lastch!='\n')) array->string[word][pos]='\0'; //when end of line is EOF
    }

    fclose(fp);
}

void readcsvlined(char* filename, int line, int readlines, struct aoc_arrayd *array) {
    struct aoc_arrays tmpstr;
    aoc_arrays_init(&tmpstr);
    readcsvlines(filename,line,readlines,&tmpstr);
    array->N=tmpstr.N;
    array->data=(int*)malloc(sizeof(int)*array->N);
    arrays2arrayd(tmpstr.string,array->data,tmpstr.N);

    
    aoc_arrays_free(&tmpstr);
}

void arrays2arrayd(char **string, int *integers, int arraysize) {
    int i;
    for (i=0; i<arraysize; i++) {
        integers[i]=atoi(string[i]);}
}

void skiplines(FILE *fp, int Nskip) {
    int i=0; 
    char ch;
    do {
        if (i==Nskip) break;
        ch=getc(fp);
        if (ch=='\n') i++;
    } while(ch!=EOF);

    if (i!=Nskip) {
        printf("something went wrong. Couldn't advance %d lines\n",Nskip);
        exit(1);
    }
}

void gotoline(FILE *fp, int lineno) {
    int i=1; //we start numbering at line1
    char ch;
    rewind(fp); //go back to the start of the file
    do {
        if (i==lineno) break;
        ch=getc(fp);
        if (ch=='\n') i++;
    } while(ch!=EOF);

    if (i!=lineno) {
        printf("something went wrong. Couldn't go to line %d \n",lineno);
        exit(1);
    }
}

void readNstrings(char *filename, char **data, int startline, int Nstrings) {
    FILE *fp;
    int i;

    if ((fp=fopen(filename,"r"))==NULL) {
        printf("ERROR: file cannnot be opened\n");
        exit(1);
    }

    gotoline(fp,startline);

    for (i=0; i<Nstrings; i++) fscanf(fp,"%s",data[i]);

    fclose(fp);
}