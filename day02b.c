/* compile with
gcc -g -Wall aocfunc.c dayxx.c -o dayxx.o
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aocfunc.h"

void usage(char *filename) {
    printf("USAGE:\n");
    printf("\t%s -i <input data file>\n\n",filename);
}

int main(int argc, char **argv) {
    struct aoc puzzle;
    int i;
    int forward=0;
    int depth=0;
    int aim=0;

    /*Check command line arguments*/
    for (i=1; i<argc; i++) {
        if (strcmp(argv[i],"-i")==0) puzzle.filename=argv[i+1];
    }
    if (puzzle.filename==NULL) {
        usage(argv[0]);
        exit(1);
    }   

    aocinit(&puzzle);

    /*read in the puzzle data*/
    readarrays(&puzzle);

    for (i=0; i<puzzle.N; i=i+2) {
        //first word is direction second word is magnitude
        if (strcmp(puzzle.pistring[i],"forward")==0) {
            forward+=atoi(puzzle.pistring[i+1]);
            depth+=aim*atoi(puzzle.pistring[i+1]);
        }
        if (strcmp(puzzle.pistring[i],"back")==0) {
            forward-=atoi(puzzle.pistring[i+1]);
        }

        if (strcmp(puzzle.pistring[i],"up")==0) {
            aim-=atoi(puzzle.pistring[i+1]);
        }  
        if (strcmp(puzzle.pistring[i],"down")==0) {
            aim+=atoi(puzzle.pistring[i+1]);
        }              
    }

    printf("Forward direction: %d  Depth=%d\n",forward,depth);
    printf("Product=%d\n",forward*depth);

    aocfree(&puzzle);

return 0;
}


