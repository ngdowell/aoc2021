#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aocfunc.h"

/*
count the number of times a depth measurement increases from the previous measurement. (There is no measurement before the first measurement.)
*/

void usage(char *filename) {
    printf("USAGE:\n");
    printf("\t%s -i <input data file>\n\n",filename);
}

int main(int argc, char **argv) {
    struct aoc puzzle;
    int i;
    int ninc=0; //the number of increases from the previous array element

    /*Check command line arguments*/
    for (i=1; i<argc; i++) {
        if (strcmp(argv[i],"-i")==0) puzzle.filename=argv[i+1];
    }
    if (puzzle.filename==NULL) {
        usage(argv[0]);
        exit(1);
    }   

    /*read in the puzzle data*/
    readarrayd(&puzzle);

    for (i=1; i<puzzle.N; i++) {
        if (puzzle.pidata[i]>puzzle.pidata[i-1]) ninc++;
    }

    printf("Number of increases: %d\n",ninc);

    aocfree(&puzzle);

return 0;
}


