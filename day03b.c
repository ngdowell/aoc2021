#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aocfunc.h"

char targetbitOGR(struct aoc *pi, int *index, int pos);
char targetbitCSR(struct aoc *pi, int *index, int pos);

void usage(char *filename) {
    printf("USAGE:\n");
    printf("\t%s -i <input data file>\n\n",filename);
}

int main(int argc, char **argv) {
    struct aoc puzzle;
    int i,j;
    char target;
    int OGR,CSR; 
    int sum=0;
    int gamma, epsilon;
    const int wordlength=12;
    char gammabin[wordlength];
    char epsilonbin[wordlength];
    int *isOGR; //bool 0 = word at that index is not OGR
    int *isCSR;

    /*Check command line arguments*/
    for (i=1; i<argc; i++) {
        if (strcmp(argv[i],"-i")==0) puzzle.filename=argv[i+1];
    }
    if (puzzle.filename==NULL) {
        usage(argv[0]);
        exit(1);
    }

    aocinit(&puzzle);
    

    readarrays(&puzzle);

    for (i=0; i<wordlength; i++) {
        sum=0;
        for (j=0; j<puzzle.N; j++) {
            if (puzzle.pistring[j][i]=='1') sum++;
        }
        if (sum>0.5*puzzle.N) gammabin[i]='1';
        else gammabin[i]='0';
    }
    gammabin[wordlength]='\0';

    /*invert the binary*/
    for (i=0; i<wordlength; i++) {
        if (gammabin[i]=='1') epsilonbin[i]='0';
        if (gammabin[i]=='0') epsilonbin[i]='1';
    }
    epsilonbin[wordlength]='\0';

    gamma=bin2dec(gammabin,wordlength);
    epsilon=bin2dec(epsilonbin,wordlength);

    printf("gammabin=%s * espisonibin=%s = %d*%d=%d\n",
        gammabin,epsilonbin,gamma,epsilon,gamma*epsilon);

    /*now work out OGR and CSR*/
    isOGR=(int*)malloc(sizeof(int)*puzzle.N);
    isCSR=(int*)malloc(sizeof(int)*puzzle.N);
    
    for (i=0; i<puzzle.N; i++) { /*initialize the bool array - any might be OGR CSR*/
        isOGR[i]=1;
        isCSR[i]=1;
    }

    /*find OGR*/
    for (i=0; i<wordlength; i++) {
        target=targetbitOGR(&puzzle,isOGR,i);
        for (j=0; j<puzzle.N; j++) {
            if (puzzle.pistring[j][i]!=target) isOGR[j]=0;
        }
        if (sumarrayd(isOGR,puzzle.N)==1) {
            printf("only 1 string fits OGR\n");
            break; 
        }
    }

    if (sumarrayd(isOGR,puzzle.N)!=1) {
        printf("didn't find OGR. quitting.\n");
        exit(1);
    }

        /*find CSR*/
    for (i=0; i<wordlength; i++) {
        target=targetbitCSR(&puzzle,isCSR,i);
        for (j=0; j<puzzle.N; j++) {
            if (puzzle.pistring[j][i]!=target) isCSR[j]=0;
        }
        if (sumarrayd(isCSR,puzzle.N)==1) {
            printf("only 1 string fits CSR\n");
            break; 
        }
    }

    if (sumarrayd(isCSR,puzzle.N)!=1) {
        printf("didn't find CSR. quitting.\n");
        exit(1);
    }

    for (j=0; j<puzzle.N; j++) {
        if (isOGR[j]==1) OGR=bin2dec(puzzle.pistring[j],wordlength);
        if (isCSR[j]==1) CSR=bin2dec(puzzle.pistring[j],wordlength);
    }

    printf("OGR= %d  CSR= %d Product= %d\n",OGR,CSR,OGR*CSR);

    aocfree(&puzzle);
    free(isOGR);
    free(isCSR);
return 0;
}



char targetbitOGR(struct aoc *pi, int *index, int pos) {
    int i,sum=0;
    int words=sumarrayd(index,pi->N);
    for (i=0; i<pi->N; i++) {
        if ( (index[i]==1) && (pi->pistring[i][pos]=='1') ) sum++;
    }
    if (sum<0.5*words) return '0';
    else return '1';
}

char targetbitCSR(struct aoc *pi, int *index, int pos) {
    int i,sum=0;
    int words=sumarrayd(index,pi->N);
    for (i=0; i<pi->N; i++) {
        if ( (index[i]==1) && (pi->pistring[i][pos]=='1') ) sum++;
    }
    if (sum<0.5*words) return '1';
    else return '0';
}