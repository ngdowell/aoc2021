CC=gcc
CFLAGS=-Wall -pedantic -g

INC=-I/usr/include
LIBS=-lm

SRC=$(wildcard day*.c)
OBJ=$(SRC:.c=.o)

all: $(OBJ)

$(OBJ):
	$(CC) $(CFLAGS) aocfunc.c $(@:.o=.c) -o $(@) $(INC) $(LIBS)

clean:
	rm -rf *.o $(EXE)