#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aocfunc.h"

int checkwin(int *board,int Nrows, int Ncols);
int summatches(int *board, int *index, int N);

void usage(char *filename) {
    printf("USAGE:\n");
    printf("\t%s -i <input data file>\n\n",filename);
}

int main(int argc, char **argv) {
    struct aoc puzzle;
    int i,j,k;
    struct aoc_arrayd numbers; //string array holding CSV data
    char **tmpstr;
    const int Nrows=5, Ncols=5; //size of bingo matrix
    const int N=Nrows*Ncols;
    const int Nboards=100; //number of bingo boards
    //const int Nboards=3; //for test data
    int line;
    int **board;
    int boardtot;
    int **winindex;
    int win=FALSE;

    /*Check command line arguments*/
    for (i=1; i<argc; i++) {
        if (strcmp(argv[i],"-i")==0) puzzle.filename=argv[i+1];
    }
    if (puzzle.filename==NULL) {
        usage(argv[0]);
        exit(1);
    }

    aoc_arrayd_init(&numbers);
    aocinit(&puzzle);

    //read in the bingo numbers from line 1 only
    readcsvlined(puzzle.filename,1,1,&numbers);

    /*allocate temporary array holding string of the board*/ 
    tmpstr=(char**)malloc(sizeof(char*)*N);
    for (i=0;i<N;i++) tmpstr[i]=(char*)malloc(sizeof(char)*500);

    /*allocate memory of 2D array all boards*/
    board=(int**)malloc(sizeof(int*)*Nboards);
    for (i=0;i<Nboards;i++) board[i]=(int*)malloc(sizeof(int)*N);

    /*the win index records 0 or 1 for position in board that have number picked*/
    winindex=(int**)malloc(sizeof(int*)*Nboards);
    for (i=0;i<Nboards;i++) winindex[i]=(int*)malloc(sizeof(int)*N);
    for (i=0;i<Nboards;i++) {
        for (j=0; j<N; j++) winindex[i][j]=0; //initialize all to zero
    }

    /* read in the bingo boards and conv string to int*/
    for (i=0; i<Nboards; i++) {
        line=3+i*6; //1st board starts on line 3 and each board takes 6 lines
        readNstrings(puzzle.filename,tmpstr,line,N); //read as stings
        arrays2arrayd(tmpstr,board[i],N);
    }

    
/* go through all the numbers and checking off matches*/
    printf("Call: ");
    for (i=0;i<numbers.N;i++) {
        printf("%d ",numbers.data[i]);
        for (j=0; j<Nboards; j++) {
            for (k=0; k<N; k++) if (board[j][k]==numbers.data[i]) winindex[j][k]=1; //set index to one for match
            if (checkwin(winindex[j],Nrows,Ncols)==1) {
                win=TRUE;
                break;
            }
        }
        if (win==TRUE) {
            printf("\nBoard %d wins\n",j);
            boardtot=sumarrayd(board[j],N)-summatches(board[j],winindex[j],N); //sum of non-matched nos
            printf("Answer: %d * %d = %d\n",boardtot,numbers.data[i],boardtot*numbers.data[i]);
            break;
        }
    }
    printf("\n");

    

    for (i=0;i<N;i++) free(tmpstr[i]);
    free(tmpstr);
    for (i=0;i<Nboards;i++) {
        free(board[i]);
        free(winindex[i]);
    }
    free(board);
    free(winindex);

    aoc_arrayd_free(&numbers);
    aocfree(&puzzle);

return 0;
}

int checkwin(int *board,int Nrows, int Ncols) {
    /*check if a row or column is bingo*/
    int i,j;
    int rowtot,coltot;

    for (i=0;i<Nrows; i++) {
        rowtot=0;
        for (j=0; j<Ncols; j++) {
            rowtot+=board[i*Ncols+j];
            if (rowtot==Ncols) {
                printf("Bingo. Row %d\n",i);
                return 1;
            }
        }
    }

    for (j=0;j<Ncols; j++) {
        coltot=0;
        for (i=0; i<Nrows; i++) {
            coltot+=board[i*Ncols+j];
            if (coltot==Nrows) {
                printf("Bingo. Col %d\n",i);
                return 1;
            }
        }
    }

    return 0; //no match
}

int summatches(int *board, int *index, int N) {
    int i;
    int total=0;

    for (i=0; i<N ; i++) total+=board[i]*index[i];

    return total;
}