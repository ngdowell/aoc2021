#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aocfunc.h"

void usage(char *filename) {
    printf("USAGE:\n");
    printf("\t%s -i <input data file>\n\n",filename);
}

int main(int argc, char **argv) {
    struct aoc puzzle;
    int i,j;
    int sum=0;
    int gamma, epsilon;
    const int wordlength=12;
    char gammabin[wordlength];
    char epsilonbin[wordlength];

    /*Check command line arguments*/
    for (i=1; i<argc; i++) {
        if (strcmp(argv[i],"-i")==0) puzzle.filename=argv[i+1];
    }
    if (puzzle.filename==NULL) {
        usage(argv[0]);
        exit(1);
    }

    aocinit(&puzzle);

    readarrays(&puzzle);

    for (i=0; i<wordlength; i++) {
        sum=0;
        for (j=0; j<puzzle.N; j++) {
            if (puzzle.pistring[j][i]=='1') sum++;
        }
        if (sum>0.5*puzzle.N) gammabin[i]='1';
        else gammabin[i]='0';
    }
    gammabin[wordlength]='\0';

    /*invert the binary*/
    for (i=0; i<wordlength; i++) {
        if (gammabin[i]=='1') epsilonbin[i]='0';
        if (gammabin[i]=='0') epsilonbin[i]='1';
    }
    epsilonbin[wordlength]='\0';

    gamma=bin2dec(gammabin,wordlength);
    epsilon=bin2dec(epsilonbin,wordlength);

    printf("gammabin=%s * espisonibin=%s = %d*%d=%d\n",
        gammabin,epsilonbin,gamma,epsilon,gamma*epsilon);

    aocfree(&puzzle);

return 0;
}